import mongoose from "mongoose";
const { Schema } = mongoose;

const reqString = { type: String, required: true };

const UserSchema = new Schema({
  name: reqString,
  email: reqString,
  password: reqString,
  gender: reqString,
  age: reqString,
  location: {
    latitude: reqString,
    longtitude: reqString,
  },
  interests: { type: [String] },
  img: [{ data: Buffer, contentType: String }],
  about: { type: String },
  id: Schema.Types.ObjectId || Number,
  liked: [],
});

const User = mongoose.model("User", UserSchema);

export default User;
