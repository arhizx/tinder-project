"use strict";
import express from "express";
const router = express.Router();
import User from "../schema/User.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { check, validationResult } from "express-validator";

const authenticateToken = (req, res, next) => {
  //get jwt token from headers
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  //check if there is one
  if (token === null) {
    return res.status(403).json({ msg: "Please login" });
  }

  //authenticate user
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.sendStatus(403).json({ msg: "Please login" });
    }

    req.user = user;
    next();
  });
};

//@@ REGISTER USER
router.post(
  "/register",
  [
    //validate fields
    check("name").isLength({ min: 2, max: 35 }),
    check("email").normalizeEmail().isEmail(),
    check("password").isLength({ min: 8 }),
    check("age").toInt().isInt({ min: 18, max: 99 }),
    check("location").exists(),
    check("gender").exists(),
    check("interests").isArray({ max: 10 }).optional(),
    check("img").isArray({ max: 5 }).optional(),
    check("about").isLength({ max: 150 }).optional(),
  ],
  async (req, res) => {
    //validate user data
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      //get the data from the request
      let {
        name,
        email,
        password,
        gender,
        age,
        location,
        interests,
        img,
        about,
      } = req.body;

      //gen hashed password
      const hashedPassword = await bcrypt.hash(password, 10);
      //check if user already exists
      const alreadyRegisteredUser = await User.findOne({ email });
      if (alreadyRegisteredUser) {
        return res.status(201).json({ msg: "user already exists" });
      }

      //create new User
      const user = new User({
        name,
        email,
        password: hashedPassword,
        gender,
        age: String(age),
        location,
        interests,
        img,
        about,
      });
      //save

      await user.save();
      return res.status(200).json("registered!");
    } catch (err) {
      console.log(err.message);
      return res.status(500).json({ msg: err.message });
    }
  }
);

//@@LOGIN USER
router.post(
  "/login",
  [
    check("email").normalizeEmail().isEmail(),
    check("password").isLength({ minLength: 8 }),
  ],
  async (req, res) => {
    //validate user data
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      //get the data
      let { email, password } = req.body;
      //find user
      const user = await User.findOne({ email });
      //check if user exists
      if (!user) {
        return res.status(401).json("User doesn`t exist, please register");
      }
      //compare the passwords
      let compare = await bcrypt.compare(password, user.password);
      if (!compare) {
        return res.status(401).json("invalid credentials");
      }
      //create access token
      const accessToken = jwt.sign(
        {
          userName: user.name,
          userId: user._id,
          userInterests: user.interests,
          userLocation: user.location,
        },
        process.env.ACCESS_TOKEN_SECRET
      );
      return res.status(200).json({ accessToken });
    } catch (e) {
      return res.status(500).json({ msg: e.message });
    }
  }
);

//@@ GET USER
router.get("/search", authenticateToken, async (req, res) => {
  try {
    //get the search params from client
    let { minAge, maxAge, genderParam } = req.query;

    //get location and interests from the token
    const { userLocation, userInterests, userId } = jwt.decode(
      req.headers["authorization"].split(" ")[1]
    );

    //TODO
    //check how location parser work

    //find user matching search params
    //here goes nasty aggregation/query mongodb syntax, you might need to see the docs to get it better
    const user = await User.aggregate([
      { $match: { interests: { $elemMatch: { $in: userInterests } } } },

      {
        $match: {
          $expr: {
            $and: [{ $gte: ["$age", minAge] }, { $lte: ["$age", maxAge] }],
          },
        },
      },

      { $match: { gender: genderParam } },

      { $match: { $expr: { $ne: ["$_id", userId] } } },

      { $sample: { size: 1 } },
    ]);

    if (!user) {
      return res.status(404).json("No matched interests user is found");
    }
    return res.status(200).json(user);
  } catch (e) {
    console.log(e.message);
    return res.status(500).json({ msg: e.message });
  }
});

//@@UPDATE USER
router.patch(
  "/update",
  [check("interests").isArray({ min: 1, max: 99 }).optional()],
  authenticateToken,
  async (req, res) => {
    try {
      //validate user data
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      //get id from token
      const { userId } = jwt.decode(req.headers["authorization"].split(" ")[1]);

      //get user
      const user = await User.findById(userId);
      //compare password
      const passwordCompare = await bcrypt.compare(
        req.body.password,
        user.password
      );

      if (!passwordCompare) {
        return res.status(403).json({ msg: "Invalid password" });
      }

      //get the data and put it in the object to update user
      let { name, email, gender, age, location, interests, about, img } =
        req.body;

      const fieldsToUpdate = {
        name,
        email,
        gender,
        age,
        location,
        interests,
        about,
        img,
      };

      //iterate over current user and update existing fields
      for (const field in fieldsToUpdate) {
        if (fieldsToUpdate[field] && user[field]) {
          user[field] = fieldsToUpdate[field];
        }
      }

      //save updates
      await user.save();

      return res.status(200).json({ msg: `User ${user.name} updated` });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
);

//@@DELETE USER
router.delete("/delete", authenticateToken, async (req, res) => {
  try {
    //get id from token
    const { userId } = jwt.decode(req.headers["authorization"].split(" ")[1]);
    //get user

    const user = await User.findById(userId);

    //for the strange scenario if someone gets to this route but the user is already deleted
    if (!user) {
      return res.status(404).json({ msg: "User not found" });
    }

    //compare password
    const passwordCompare = await bcrypt.compare(
      req.body.password,
      user.password
    );

    if (!passwordCompare) {
      return res.status(403).json({ msg: "Invalid password" });
    }

    await user.delete();

    return res.status(200).json({ msg: `User ${user.name} deleted` });
  } catch (err) {
    console.log(err.message);
    return res.status(500).json({ msg: err.message });
  }
});

export default router;
