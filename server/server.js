import testServer from "./testServer.js";
const PORT = process.env.PORT || 3000;

// listening to the port
testServer.listen(PORT, () => {
  console.log(`server is running on ${PORT}`);
});
