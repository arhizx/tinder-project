"use strict";
import dotenv from "dotenv";
dotenv.config();
import express from "express";
import mongoose from "mongoose";
import user from "./routes/user.js";
const server = express();

// parse application/json
server.use(express.json());
//db connection
mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

server.use("/user", user);

export default server;
