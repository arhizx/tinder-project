import request from "supertest";
import server from "./server/testServer.js";
import jwt from "jsonwebtoken";

jest.setTimeout(100000);
const testUser = {
  id: 1,
  name: "Boddl",
  email: "twerky@gmail.com",
  password: "12345wsrt",
  gender: "male",
  age: "25",
  location: { latitude: "134.4545", longtitude: "1212332.44" },
  interests: ["aliquip", "consequat", "do", "in"],
  about: "imma guddly doodly",
};
let token = null;
let testUserId = null;
//dunno if i should put comment in here, it`s basic syntax and some tests, that`s it

describe("User routes", () => {
  test("should successfully register user", async () => {
    const response = await request(server)
      .post("/user/register")
      .set("Content-type", "application/json")
      .send(testUser);

    expect(response.statusCode).toBe(200);
  });

  test("should succesfully login user", async () => {
    let { email, password } = testUser;
    const response = await request(server)
      .post("/user/login")
      .set("Content-type", "application/json")
      .send({ email, password });

    token = response.body.accessToken;
    testUserId = jwt.decode(token).userId;

    expect(response.statusCode).toBe(200);
  });

  test("should return relevant user depending on the query parameters", async () => {
    const params = { minAge: 18, maxAge: 25, genderParam: "female" };

    const response = await request(server)
      .get("/user/search")
      .set("Authorization", `Bearer ${token}`)
      .set("Content-type", "application/json")
      .query(params);

    expect(response.statusCode).toBe(200);

    console.log(response.body);
    const matchedUser = response.body[0];
    let { _id, age, gender, interests } = matchedUser;
    const checkMatchedUser = () => {
      switch (true) {
        case _id === testUserId:
          throw new Error("it`s the same user that wants to search, incorrect");
        case age < params.minAge && age > maxAge:
          throw new Error("age isn`t correct");
        case gender !== params.genderParam:
          throw new Error("gender isn`t correct");
      }
      return true;
    };

    expect(checkMatchedUser());
  });

  test("should successfully delete user", async () => {
    let { password } = testUser;
    const response = await request(server)
      .delete("/user/delete")
      .set("Authorization", `Bearer ${token}`)
      .set("Content-type", "application/json")
      .send({ password });

    expect(response.statusCode).toBe(200);
  });
});
